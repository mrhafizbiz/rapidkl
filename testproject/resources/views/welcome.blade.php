<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Routes Visualization</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
    <style>
    #map {
        height: 600px;
        width: 100%;
    }

    #routeSelector,
    #searchRoute {
        margin-bottom: 20px;
    }

    #stopTimesTable {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }

    #stopTimesTable th,
    #stopTimesTable td {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #stopTimesTable th {
        background-color: #f2f2f2;
        text-align: left;
    }
    </style>
</head>

<body>
    <div>
        <input type="text" id="searchRoute" placeholder="Search Route">
        <select id="routeSelector">
            <option value="">Select Route</option>
        </select>
    </div>
    <div id="map"></div>
    <table id="stopTimesTable">
        <thead>
            <tr>
                <th>Stop Name</th>
                <th>Arrival Time</th>
                <th>Departure Time</th>
                <th>Stop Headsign</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        const map = L.map('map').setView([3.134093, 101.753511], 13);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        // Fetch available routes for dropdown
        let routesData = [];
        fetch('http://127.0.0.1:8000/api/getRoute')
            .then(response => response.json())
            .then(data => {
                routesData = data.message.route;
                populateRouteSelector(routesData);
            });

        document.getElementById('routeSelector').addEventListener('change', function() {
            const selectedRouteId = this.value;
            if (selectedRouteId) {
                fetchRouteData(selectedRouteId);
            }
        });

        document.getElementById('searchRoute').addEventListener('input', function() {
            const searchText = this.value.toLowerCase();
            const filteredRoutes = routesData.filter(route =>
                route.route_long_name.toLowerCase().includes(searchText) ||
                route.route_short_name.toLowerCase().includes(searchText)
            );
            populateRouteSelector(filteredRoutes);
        });

        function populateRouteSelector(routes) {
            const routeSelector = document.getElementById('routeSelector');
            routeSelector.innerHTML = '<option value="">Select Route</option>'; // Clear previous options
            routes.forEach(route => {
                const option = document.createElement('option');
                option.value = route.route_id;
                option.text = `${route.route_short_name} - ${route.route_long_name}`;
                routeSelector.add(option);
            });
        }

        function fetchRouteData(routeId) {
            fetch(`http://127.0.0.1:8000/api/getRouteWithTrip?route_id=${routeId}`)
                .then(response => response.json())
                .then(data => {
                    map.eachLayer(layer => {
                        if (layer instanceof L.Polyline) {
                            map.removeLayer(layer);
                        }
                    });

                    data.message.route.forEach(route => {
                        if (route.trip) {
                            route.trip.forEach(trip => {
                                const coordinates = trip.stop_time.map(stopTime => [
                                    parseFloat(stopTime.stop.stop_lat),
                                    parseFloat(stopTime.stop.stop_lon)
                                ]);
                                const polyline = L.polyline(coordinates, {
                                    color: '#' + route.route_color
                                }).addTo(map);
                                map.fitBounds(polyline.getBounds());

                                populateStopTimesTable(trip.stop_time);
                            });
                        }
                    });
                });
        }

        function populateStopTimesTable(stopTimes) {
            const tableBody = document.getElementById('stopTimesTable').getElementsByTagName('tbody')[0];
            tableBody.innerHTML = ''; // Clear previous table data

            stopTimes.forEach(stopTime => {
                const row = document.createElement('tr');
                row.innerHTML = `
                        <td>${stopTime.stop.stop_name}</td>
                        <td>${stopTime.arrival_time}</td>
                        <td>${stopTime.departure_time}</td>
                        <td>${stopTime.stop_headsign}</td>
                    `;
                tableBody.appendChild(row);
            });
        }
    });
    </script>
</body>

</html>