document.addEventListener("DOMContentLoaded", function () {
    // Initialize the map
    var map = L.map("map").setView([37.7749, -122.4194], 13); // Centered on San Francisco

    // Set up the OSM layer
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 19,
    }).addTo(map);

    // Fetch routes data
    fetch("http://127.0.0.1:8000/api/getRouteWithTrip")
        .then((response) => response.json())
        .then((data) => {
            data.forEach((route) => {
                route.trips.forEach((trip) => {
                    // Assuming stopTimes are ordered by stop_sequence
                    const latlngs = trip.stopTimes.map((stopTime) => [
                        stopTime.latitude,
                        stopTime.longitude,
                    ]);

                    // Draw the polyline for the route
                    L.polyline(latlngs, {
                        color: route.route_color || "blue",
                    }).addTo(map);

                    // Add markers for each stop
                    trip.stopTimes.forEach((stopTime) => {
                        L.marker([stopTime.latitude, stopTime.longitude])
                            .bindPopup(
                                `<b>Stop:</b> ${stopTime.stop_id}<br><b>Arrival:</b> ${stopTime.arrival_time}<br><b>Departure:</b> ${stopTime.departure_time}`
                            )
                            .addTo(map);
                    });
                });
            });
        });
});
