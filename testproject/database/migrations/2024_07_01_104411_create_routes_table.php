<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('route_id')->nullable();
            $table->string('agency_id'); // Foreign key to agencies table
            $table->string('route_short_name')->nullable();
            $table->string('route_long_name')->nullable();
            $table->string('route_type')->nullable();
            $table->string('route_color')->nullable();
            $table->string('route_text_color')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('routes');
    }
};
