<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('service_id')->nullable();
            $table->enum('monday', [0, 1])->nullable();
            $table->enum('tuesday', [0, 1])->nullable();
            $table->enum('wednesday', [0, 1])->nullable();
            $table->enum('thursday', [0, 1])->nullable();
            $table->enum('friday', [0, 1])->nullable();
            $table->enum('saturday', [0, 1])->nullable();
            $table->enum('sunday', [0, 1])->nullable();
            $table->string('start_date', 8); // To store date as YYYYMMDD
            $table->string('end_date', 8); // To store date as YYYYMMDD
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('calendars');
    }
};
