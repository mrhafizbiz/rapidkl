<?php

namespace App\Http\Controllers\BusTrip;

use App\Http\Controllers\Controller;
use App\Models\Trip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TripController extends Controller
{
    public function getTrip()
    {
        $response = Trip::getTrip();
        return $response;
    }

    public function getTripWithStopTime(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trip_id' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 422, 'errors' => $validator->errors(), 'message' => 'Failed. '], 422);
        }

        $response = Trip::getTripWithStopTime($request['trip_id']);
        return $response;
    }
}
