<?php

namespace App\Http\Controllers\GTFSService;

use App\Http\Controllers\Controller;
use App\Services\GTFSService;

class GTFSController extends Controller
{
    protected $gtfsService;

    public function __construct(GTFSService $gtfsService)
    {
        $this->gtfsService = $gtfsService;
    }

    public function fetchGTFSData()
    {
        $this->gtfsService->fetchAndStoreGTFSData();
        return response()->json(['status' => 200, 'message' => 'GTFS data fetched and stored successfully.'], 200);
    }
}
