<?php

namespace App\Http\Controllers\BusRoute;

use App\Http\Controllers\Controller;
use App\Models\Agency;
use App\Models\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RouteController extends Controller
{
    public function getRoute()
    {
        $response = Route::getRoute();
        return $response;
    }

    public function getAgency(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 422, 'errors' => $validator->errors(), 'message' => 'Failed. '], 422);
        }

        $response = Agency::getAgencyWithRoute($request['id']);
        return $response;
    }

    public function getRouteWithTrip(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'route_id' => ['required'],
            'search' => ['nullable', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 422, 'errors' => $validator->errors(), 'message' => 'Failed. '], 422);
        }

        $response = Route::getRouteWithTrip($request);
        return $response;
    }
}
