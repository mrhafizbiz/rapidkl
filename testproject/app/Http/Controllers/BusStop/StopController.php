<?php

namespace App\Http\Controllers\BusStop;

use App\Http\Controllers\Controller;
use App\Models\Stop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StopController extends Controller
{
    public function getStop()
    {
        $response = Stop::getStop();
        return $response;
    }

    public function getStopWithTime(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'stop_id' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 422, 'errors' => $validator->errors(), 'message' => 'Failed. '], 422);
        }

        $response = Stop::getStopWithStopTime($request['stop_id']);
        return $response;
    }
}
