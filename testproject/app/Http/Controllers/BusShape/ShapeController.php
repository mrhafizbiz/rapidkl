<?php

namespace App\Http\Controllers\BusShape;

use App\Http\Controllers\Controller;
use App\Models\Shape;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ShapeController extends Controller
{
    public function getShape()
    {
        $response = Shape::getShape();
        return $response;
    }

    public function getShapeWithTrip(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shape_id' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 422, 'errors' => $validator->errors(), 'message' => 'Failed. '], 422);
        }

        $response = Shape::getShapeWithTrip($request['shape_id']);
        return $response;
    }
}
