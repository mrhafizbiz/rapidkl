<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stop extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'stop_id',
        'stop_name',
        'stop_desc',
        'stop_lat',
        'stop_lon',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function stopTime()
    {
        return $this->hasMany(StopTime::class, 'stop_id', 'stop_id');
    }

    public static function getStop()
    {
        $stop = Stop::get();
        if (count($stop) == 0) {
            $errors['stop'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['stop'] = $stop;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }

    public static function getStopWithStopTime($request)
    {
        $stop = Stop::where('stop_id', $request)->with('stopTime')->get();
        if (count($stop) == 0) {
            $errors['stop'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['stop'] = $stop;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }
}
