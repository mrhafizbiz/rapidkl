<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'agency_id',
        'agency_name',
        'agency_url',
        'agency_timezone',
        'agency_phone',
        'agency_lang',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function routes()
    {
        return $this->hasMany(Route::class, 'agency_id', 'agency_id');
    }

    public static function getAgencyWithRoute($request)
    {
        $agencies = Agency::where('id', $request)->with('routes')->get();
        if (count($agencies) == 0) {
            $errors['agencies'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['agencies_routes'] = $agencies;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }
}
