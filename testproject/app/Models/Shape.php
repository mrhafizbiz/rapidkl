<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shape extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'shape_id',
        'shape_pt_lat',
        'shape_pt_lon',
        'shape_pt_sequence',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function trip()
    {
        return $this->hasMany(Trip::class, 'shape_id', 'shape_id');
    }

    public static function getShape()
    {
        $shape = Shape::get();
        if (count($shape) == 0) {
            $errors['shape'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['shape'] = $shape;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }

    public static function getShapeWithTrip($request)
    {
        $shape = Shape::where('shape_id', $request)->with('trip')->get();
        if (count($shape) == 0) {
            $errors['shape'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['shape'] = $shape;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }
}
