<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Frequency extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'trip_id',
        'start_time',
        'end_time',
        'headway_secs',
        'exact_times',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'start_time' => 'datetime:H:i:s',
        'end_time' => 'datetime:H:i:s',
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];
}
