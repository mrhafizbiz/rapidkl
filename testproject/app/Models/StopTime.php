<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StopTime extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'trip_id',
        'arrival_time',
        'departure_time',
        'stop_id',
        'stop_sequence',
        'stop_headsign',
    ];

    protected $hidden = [
        'arrival_time' => 'datetime:H:i:s',
        'departure_time' => 'datetime:H:i:s',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function stop()
    {
        return $this->belongsTo(Stop::class, 'stop_id', 'stop_id');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id', 'trip_id');
    }
}
