<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'route_id',
        'agency_id',
        'route_short_name',
        'route_long_name',
        'route_type',
        'route_color',
        'route_text_color',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agency_id', 'agency_id');
    }

    public function trip()
    {
        return $this->hasMany(Trip::class, 'route_id', 'route_id');
    }

    public static function getRoute()
    {
        $route = Route::get();
        if (count($route) == 0) {
            $errors['route'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['route'] = $route;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }

    public static function getRouteWithTrip($request)
    {
        $query = Route::with('trip', 'trip.stopTime', 'trip.stopTime.stop');

        if ($request->has('route_id')) {
            $routeId = $request->input('route_id');
            $query->where('route_id', '=', $routeId);
        }

        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $query->orwhere('route_long_name', 'like', '%' . $searchTerm . '%');
        }

        $route = $query->get();

        if ($route->isEmpty()) {
            $errors['route'] = ["No routes found matching the search criteria."];
            return response()->json(['status' => 404, 'errors' => $errors, 'message' => 'No routes found.'], 404);
        }

        $data['route'] = $route;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }
}
