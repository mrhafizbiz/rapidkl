<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trip extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'route_id',
        'service_id',
        'trip_id',
        'shape_id',
        'trip_headsign',
        'direction_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s',
        'updated_at' => 'datetime:H:i:s',
        'deleted_at' => 'datetime:H:i:s',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function stopTime()
    {
        return $this->hasMany(StopTime::class, 'trip_id', 'trip_id');
    }

    public function routes()
    {
        return $this->belongsTo(Route::class, 'route_id', 'route_id');
    }

    public function shape()
    {
        return $this->belongsTo(Shape::class, 'shape_id', 'shape_id');
    }

    public static function getTrip()
    {
        $trip = Trip::get();
        if (count($trip) == 0) {
            $errors['trip'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['trip'] = $trip;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }

    public static function getTripWithStopTime($request)
    {
        $trip = Trip::where('trip_id', $request)->with('stopTime')->get();
        if (count($trip) == 0) {
            $errors['trip'] = array("Sorry. No Data. ");
            return response()->json(['status' => 403, 'errors' => $errors, 'message' => 'Failed. '], 403);
        }

        $data['trip'] = $trip;
        return response()->json(['status' => 200, 'message' => $data], 200);
    }

}
