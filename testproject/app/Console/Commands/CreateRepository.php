<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CreateRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->createRepository($name);
        $this->createRepositoryContracts($name);
    }

    protected function createRepository($name)
    {
        $repositoryClass = "{$name}Repository";
        $repositoryPath = app_path("Repositories/Eloquents/{$repositoryClass}.php");

        // Check if the repository already exists
        if (File::exists($repositoryPath)) {
            $this->error('Repository Eloquents already exists!');
            return;
        }

        // Create the repository file
        File::put($repositoryPath, $this->getEloquents($name));

        $this->info("Repository Eloquents created successfully: {$repositoryClass}");

    }

    protected function createRepositoryContracts($name)
    {
        $repositoryClass = "{$name}RepositoryInterface";
        $repositoryPath = app_path("Repositories/Contracts/{$repositoryClass}.php");

        // Check if the repository already exists
        if (File::exists($repositoryPath)) {
            $this->error('Repository Contracts already exists!');
            return;
        }

        // Create the repository file
        File::put($repositoryPath, $this->getContracts($name));

        $this->info("Repository Contracts created successfully: {$repositoryClass}");

    }

    protected function getEloquents($model)
    {
        $nameInterface = $model . 'RepositoryInterface';
        $nameClass = $model . 'Repository';
        $nameSmall = strtolower($model);

        return "<?php

    namespace App\Repositories\Eloquents;

    use App\Models\\$model;
    use App\Repositories\Contracts\\$nameInterface;

    class {$nameClass} implements {$nameInterface}
    {
        protected \${$nameSmall};

        public function __construct({$model} \${$nameSmall})
        {
            \$this->$nameSmall = \${$nameSmall};
        }

        // Example method:
        public function getAll()
        {
            return \$this->{$nameSmall}->all();
        }

        // Example method:
        // public function find(\$id)
        // {
        //     return \$this->{$nameSmall}->find(\$id);
        // }

        // Example method:
        // public function create(array \$data)
        // {
        //     return \$this->{$nameSmall}->create(\$data);
        // }

        // Example method:
        // public function update(\$id, array \$data)
        // {
        //     \$record = \$this->find(\$id);
        //     return \$record->update(\$data);
        // }

        // Example method:
        // public function delete(\$id)
        // {
        //     return \$this->{$nameSmall}->destroy(\$id);
        // }
    }
    ";
    }

    protected function getContracts($model)
    {
        $nameInterface = $model . 'RepositoryInterface';

        return "<?php

    namespace App\Repositories\Contracts;

    interface {$nameInterface}
    {
        // Example method:
        public function getAll();
        // public function find(\$id);
        // public function create(array \$data);
        // public function update(\$id, array \$data);
        // public function delete(\$id);
    }
    ";
    }

}
