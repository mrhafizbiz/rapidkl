<?php

namespace App\Jobs;

use App\Models\Frequency;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FrequencyFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath;
    /**
     * Create a new job instance.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $handle = fopen($this->filePath, 'r');
        $isFirstRow = true; // Flag to skip the first row

        $dataCreated = [];

        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($isFirstRow) {
                    $isFirstRow = false;
                    continue; // Skip processing the first row
                }

                $dataCreated[] = [
                    'trip_id' => $data[0],
                    'start_time' => $data[1],
                    'end_time' => $data[2],
                    'headway_secs' => $data[3],
                    'exact_times' => $data[4],
                ];

                // Insert in batches of 100
                if (count($dataCreated) === 100) {
                    Frequency::insert($dataCreated);
                    $dataCreated = [];
                }
            }

            // Insert remaining records
            if (!empty($dataCreated)) {
                Frequency::insert($dataCreated);
            }

            fclose($handle);
        }
    }
}
