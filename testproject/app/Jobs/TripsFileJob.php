<?php

namespace App\Jobs;

use App\Models\Trip;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TripsFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath;

    /**
     * Create a new job instance.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $handle = fopen($this->filePath, 'r');
        $isFirstRow = true; // Flag to skip the first row

        $dataCreated = [];

        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($isFirstRow) {
                    $isFirstRow = false;
                    continue; // Skip processing the first row
                }

                $dataCreated[] = [
                    'route_id' => $data[0],
                    'service_id' => $data[1],
                    'trip_id' => $data[2],
                    'shape_id' => $data[3],
                    'trip_headsign' => $data[4],
                    'direction_id' => $data[5],
                ];

                // Insert in batches of 100
                if (count($dataCreated) === 100) {
                    Trip::insert($dataCreated);
                    $dataCreated = [];
                }
            }

            // Insert remaining records
            if (!empty($dataCreated)) {
                Trip::insert($dataCreated);
            }

            fclose($handle);
        }
    }
}
