<?php

namespace App\Jobs;

use App\Models\Calendar;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CalendarFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath;

    /**
     * Create a new job instance.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $handle = fopen($this->filePath, 'r');
        $isFirstRow = true; // Flag to skip the first row

        $dataCreated = [];

        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($isFirstRow) {
                    $isFirstRow = false;
                    continue; // Skip processing the first row
                }

                $dataCreated[] = [
                    'service_id' => $data[0],
                    'monday' => $data[1],
                    'tuesday' => $data[2],
                    'wednesday' => $data[3],
                    'thursday' => $data[4],
                    'friday' => $data[5],
                    'saturday' => $data[6],
                    'sunday' => $data[7],
                    'start_date' => $data[8],
                    'end_date' => $data[9],
                ];

                // Insert in batches of 100
                if (count($dataCreated) === 100) {
                    Calendar::insert($dataCreated);
                    $dataCreated = [];
                }
            }

            // Insert remaining records
            if (!empty($dataCreated)) {
                Calendar::insert($dataCreated);
            }

            fclose($handle);
        }
    }
}
