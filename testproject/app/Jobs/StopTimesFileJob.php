<?php

namespace App\Jobs;

use App\Models\StopTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StopTimesFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath;

    /**
     * Create a new job instance.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $handle = fopen($this->filePath, 'r');
        $isFirstRow = true; // Flag to skip the first row

        $dataCreated = [];

        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($isFirstRow) {
                    $isFirstRow = false;
                    continue; // Skip processing the first row
                }

                $dataCreated[] = [
                    'trip_id' => $data[0],
                    'arrival_time' => $data[1],
                    'departure_time' => $data[2],
                    'stop_id' => $data[3],
                    'stop_sequence' => $data[4],
                    'stop_headsign' => $data[5],
                ];

                // Insert in batches of 100
                if (count($dataCreated) === 100) {
                    StopTime::insert($dataCreated);
                    $dataCreated = [];
                }
            }

            // Insert remaining records
            if (!empty($dataCreated)) {
                StopTime::insert($dataCreated);
            }

            fclose($handle);
        }
    }
}
