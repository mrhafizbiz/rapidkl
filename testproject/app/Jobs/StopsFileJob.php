<?php

namespace App\Jobs;

use App\Models\Stop;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StopsFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath;

    /**
     * Create a new job instance.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $handle = fopen($this->filePath, 'r');
        $isFirstRow = true; // Flag to skip the first row

        $dataCreated = [];

        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($isFirstRow) {
                    $isFirstRow = false;
                    continue; // Skip processing the first row
                }

                $dataCreated[] = [
                    'stop_id' => $data[0],
                    'stop_name' => $data[1],
                    'stop_desc' => $data[2],
                    'stop_lat' => $data[3],
                    'stop_lon' => $data[4],
                ];

                // Insert in batches of 100
                if (count($dataCreated) === 100) {
                    Stop::insert($dataCreated);
                    $dataCreated = [];
                }
            }

            // Insert remaining records
            if (!empty($dataCreated)) {
                Stop::insert($dataCreated);
            }

            fclose($handle);
        }
    }
}
