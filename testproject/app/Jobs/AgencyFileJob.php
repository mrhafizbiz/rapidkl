<?php

namespace App\Jobs;

use App\Models\Agency;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AgencyFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath;
    /**
     * Create a new job instance.
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $handle = fopen($this->filePath, 'r');
        $isFirstRow = true; // Flag to skip the first row

        $agencies = [];

        if ($handle !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($isFirstRow) {
                    $isFirstRow = false;
                    continue; // Skip processing the first row
                }

                $agencies[] = [
                    'agency_id' => $data[0],
                    'agency_name' => $data[1],
                    'agency_url' => $data[2],
                    'agency_timezone' => $data[3],
                    'agency_phone' => $data[4],
                    'agency_lang' => $data[5],
                ];

                // Insert in batches of 100
                if (count($agencies) === 100) {
                    Agency::insert($agencies);
                    $agencies = [];
                }
            }

            // Insert remaining records
            if (!empty($agencies)) {
                Agency::insert($agencies);
            }

            fclose($handle);
        }
    }
}
