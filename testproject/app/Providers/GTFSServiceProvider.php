<?php

namespace App\Providers;

use App\Services\GTFSService;
use Illuminate\Support\ServiceProvider;

class GTFSServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(GTFSServiceProvider::class, function ($app) {
            return new GTFSService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
