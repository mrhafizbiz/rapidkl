<?php
namespace App\Services;

use App\Jobs\AgencyFileJob;
use App\Jobs\CalendarFileJob;
use App\Jobs\FrequencyFileJob;
use App\Jobs\RoutesFileJob;
use App\Jobs\ShapesFileJob;
use App\Jobs\StopsFileJob;
use App\Jobs\StopTimesFileJob;
use App\Jobs\TripsFileJob;
use Illuminate\Support\Facades\Http;
use ZipArchive;

class GTFSService
{
    public function fetchAndStoreGTFSData()
    {
        // Step 1: Download the ZIP file
        $response = Http::get('https://api.data.gov.my/gtfs-static/prasarana?category=rapid-bus-kl');
        $zipContent = $response->body();

        // Step 2: Save the ZIP file to the public directory
        $zipPath = public_path('/prasarana/rapid/kl/gtfs.zip');
        file_put_contents($zipPath, $zipContent);

        // Step 3: Unzip the file
        $zip = new ZipArchive;
        if ($zip->open($zipPath) === true) {
            $zip->extractTo(public_path('/prasarana/rapid/kl/gtfs'));
            $zip->close();
        } else {
            throw new \Exception('Failed to unzip the GTFS file');
        }

        // Step 4: Process the extracted text files
        $this->processExtractedFiles();
    }

    protected function processExtractedFiles()
    {
        // Define paths to the extracted files in the public directory
        $agencyFile = public_path('/prasarana/rapid/kl/gtfs/agency.txt');
        $calendarFile = public_path('/prasarana/rapid/kl/gtfs/calendar.txt');
        $frequenciesFile = public_path('/prasarana/rapid/kl/gtfs/frequencies.txt');
        $routesFile = public_path('/prasarana/rapid/kl/gtfs/routes.txt');
        $shapesFile = public_path('/prasarana/rapid/kl/gtfs/shapes.txt');
        $stopTimesFile = public_path('/prasarana/rapid/kl/gtfs/stop_times.txt');
        $stopsFile = public_path('/prasarana/rapid/kl/gtfs/stops.txt');
        $tripsFile = public_path('/prasarana/rapid/kl/gtfs/trips.txt');

        AgencyFileJob::dispatch($agencyFile);
        CalendarFileJob::dispatch($calendarFile);
        FrequencyFileJob::dispatch($frequenciesFile);
        RoutesFileJob::dispatch($routesFile);
        ShapesFileJob::dispatch($shapesFile);
        StopTimesFileJob::dispatch($stopTimesFile);
        StopsFileJob::dispatch($stopsFile);
        TripsFileJob::dispatch($tripsFile);

    }

}
