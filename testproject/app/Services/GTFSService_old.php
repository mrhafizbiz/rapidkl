<?php
namespace App\Services;

use App\Jobs\AgencyFileJob;
use App\Jobs\CalendarFileJob;
use App\Jobs\FrequencyFileJob;
use App\Jobs\RoutesFileJob;
use App\Jobs\ShapesFileJob;
use App\Jobs\StopsFileJob;
use App\Jobs\StopTimesFileJob;
use App\Jobs\TripsFileJob;
use Illuminate\Support\Facades\Http;
use ZipArchive;

class GTFSService
{
    public function fetchAndStoreGTFSData()
    {
        // Step 1: Download the ZIP file
        $response = Http::get('https://api.data.gov.my/gtfs-static/prasarana?category=rapid-bus-kl');
        $zipContent = $response->body();

        // Step 2: Save the ZIP file to the public directory
        $zipPath = public_path('/prasarana/rapid/kl/gtfs.zip');
        file_put_contents($zipPath, $zipContent);

        // Step 3: Unzip the file
        $zip = new ZipArchive;
        if ($zip->open($zipPath) === true) {
            $zip->extractTo(public_path('/prasarana/rapid/kl/gtfs'));
            $zip->close();
        } else {
            throw new \Exception('Failed to unzip the GTFS file');
        }

        // Step 4: Process the extracted text files
        $this->processExtractedFiles();
    }

    protected function processExtractedFiles()
    {
        // Define paths to the extracted files in the public directory
        $agencyFile = public_path('/prasarana/rapid/kl/gtfs/agency.txt');
        $calendarFile = public_path('/prasarana/rapid/kl/gtfs/calendar.txt');
        $frequenciesFile = public_path('/prasarana/rapid/kl/gtfs/frequencies.txt');
        $routesFile = public_path('/prasarana/rapid/kl/gtfs/routes.txt');
        $shapesFile = public_path('/prasarana/rapid/kl/gtfs/shapes.txt');
        $stopTimesFile = public_path('/prasarana/rapid/kl/gtfs/stop_times.txt');
        $stopsFile = public_path('/prasarana/rapid/kl/gtfs/stops.txt');
        $tripsFile = public_path('/prasarana/rapid/kl/gtfs/trips.txt');

        AgencyFileJob::dispatch($agencyFile);
        CalendarFileJob::dispatch($calendarFile);
        FrequencyFileJob::dispatch($frequenciesFile);
        RoutesFileJob::dispatch($routesFile);
        ShapesFileJob::dispatch($shapesFile);
        StopTimesFileJob::dispatch($stopTimesFile);
        StopsFileJob::dispatch($stopsFile);
        TripsFileJob::dispatch($tripsFile);

        // $this->processAgencyFile($agencyFile);
        // $this->processCalendarFile($calendarFile);
        // $this->processFrequencyFile($frequenciesFile);
        // $this->processRoutesFile($routesFile);
        // $this->processShapesFile($shapesFile);
        // $this->processStopTimesFile($stopTimesFile);
        // $this->processStopsFile($stopsFile);
        // $this->processTripsFile($tripsFile);

    }

    // protected function processAgencyFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Agency::create([
    //                 'agency_id' => $data[0],
    //                 'agency_name' => $data[1],
    //                 'agency_url' => $data[2],
    //                 'agency_timezone' => $data[3],
    //                 'agency_phone' => $data[4],
    //                 'agency_lang' => $data[5],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processCalendarFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Calendar::create([
    //                 'service_id' => $data[0],
    //                 'monday' => $data[1],
    //                 'tuesday' => $data[2],
    //                 'wednesday' => $data[3],
    //                 'thursday' => $data[4],
    //                 'friday' => $data[5],
    //                 'saturday' => $data[6],
    //                 'sunday' => $data[7],
    //                 'start_date' => $data[8],
    //                 'end_date' => $data[9],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processFrequencyFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Frequency::create([
    //                 'trip_id' => $data[0],
    //                 'start_time' => $data[1],
    //                 'end_time' => $data[2],
    //                 'headway_secs' => $data[3],
    //                 'exact_times' => $data[4],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processRoutesFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Route::create([
    //                 'route_id' => $data[0],
    //                 'agency_id' => $data[1],
    //                 'route_short_name' => $data[2],
    //                 'route_long_name' => $data[3],
    //                 'route_type' => $data[4],
    //                 'route_color' => $data[5],
    //                 'route_text_color' => $data[6],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processShapesFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Shape::create([
    //                 'shape_id' => $data[0],
    //                 'shape_pt_lat' => $data[1],
    //                 'shape_pt_lon' => $data[2],
    //                 'shape_pt_sequence' => $data[3],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processStopTimesFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             StopTime::create([
    //                 'trip_id' => $data[0],
    //                 'arrival_time' => $data[1],
    //                 'departure_time' => $data[2],
    //                 'stop_id' => $data[3],
    //                 'stop_sequence' => $data[4],
    //                 'stop_headsign' => $data[5],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processStopsFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Stop::create([
    //                 'stop_id' => $data[0],
    //                 'stop_name' => $data[1],
    //                 'stop_desc' => $data[2],
    //                 'stop_lat' => $data[3],
    //                 'stop_lon' => $data[4],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

    // protected function processTripsFile($filePath)
    // {
    //     $handle = fopen($filePath, 'r');
    //     $isFirstRow = true; // Flag to skip the first row

    //     if ($handle !== false) {
    //         while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    //             if ($isFirstRow) {
    //                 $isFirstRow = false;
    //                 continue; // Skip processing the first row
    //             }

    //             Trip::create([
    //                 'route_id' => $data[0],
    //                 'service_id' => $data[1],
    //                 'trip_id' => $data[2],
    //                 'shape_id' => $data[3],
    //                 'trip_headsign' => $data[4],
    //                 'direction_id' => $data[5],
    //             ]);
    //         }
    //         fclose($handle);
    //     }
    // }

}
