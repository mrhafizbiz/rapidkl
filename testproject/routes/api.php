<?php

use App\Http\Controllers\BusRoute\RouteController;
use App\Http\Controllers\BusShape\ShapeController;
use App\Http\Controllers\BusStop\StopController;
use App\Http\Controllers\BusTrip\TripController;
use App\Http\Controllers\GTFSService\GTFSController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
 */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/fetchGtfsData', [GTFSController::class, 'fetchGTFSData']);

Route::get('/getAgency', [RouteController::class, 'getAgency']);

Route::get('/getRoute', [RouteController::class, 'getRoute']);
Route::get('/getRouteWithTrip', [RouteController::class, 'getRouteWithTrip']);

Route::get('/getStop', [StopController::class, 'getStop']);
Route::get('/getStopWithTime', [StopController::class, 'getStopWithTime']);

Route::get('/getTrip', [TripController::class, 'getTrip']);
Route::get('/getTripWithStopTime', [TripController::class, 'getTripWithStopTime']);

Route::get('/getShape', [ShapeController::class, 'getShape']);
Route::get('/getShapeWithTrip', [ShapeController::class, 'getShapeWithTrip']);
